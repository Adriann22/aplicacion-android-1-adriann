package com.example.huerto

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.huerto.ui.theme.HuertoTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            HuertoTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    Greeting("Android")
                }
            }
        }
    }
}

@Composable
fun Greeting(name: String, modifier: Modifier = Modifier) {
    val image = painterResource(R.drawable.huerto)
    val image2 = painterResource(R.drawable.huerto2)
    val image3 = painterResource(R.drawable.huerto3)



    var int by remember {
        mutableStateOf(1)
    }
    var total by remember {
        mutableStateOf(0)
    }

    Column() {

        if(int==1){
            Image(painter = image, contentDescription = "huerto")
        }
        if(int==2){
            Image(painter = image2, contentDescription = "huerto3")
        }
        if(int==3){
            Image(painter = image3, contentDescription = "huerto3")
            var numale = (1..5).random()
            total+=numale
        }




        Button(
            onClick =
            {
            int++
            if(int>3){
                int=1
            }

            }
        ) {
            Text("Crecer")
        }

        Text("Euros: "+ total.toString())
    }
}


@Preview(showBackground = true)
@Composable
fun GreetingPreview() {
    HuertoTheme {
        Greeting("Android")
    }
}